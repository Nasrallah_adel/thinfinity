// =========================================================================================================================
// Copyright 2014 - 2016, CybeleSoft
// CYBELESOFT
// 20151028.1
// info@cybelesoft.com
// =========================================================================================================================

var mythinrdp = null;

$(document).ready(function () {
    // -- Get login security methods.
    // -- Until the new method is done, use login with blans credentials to get methods.
    mythinrdp = new GetThinRDP();

    // -- Define login function
    function doLogin() {
        $.blockUI({
            message: "<div class='popup' id='establishingConnection'><h2>" + thinRDPconsts.pleaseWait + "</h2><img id='ecWaiting' src='../images/core/loadajax.gif' /></div>",
            css: {
                'padding': 0, 'margin': 0,
                'width': '100%', 'height': '100%',
                'top': '0px', 'left': '0px',
                'textAlign': 'center',
                'backgroundColor': 'transparent',
                'border': 'none',
                'cursor': 'auto'
            }, centerX: true, centerY: true
        });
        mythinrdp.login($("#userid").val(), $("#password").val(), $("#secMethod").val(),
            // -- Success
            function (response) {
                location.reload();
            },
            // -- Failure
            function (response) {
                $("#password").val("");
                $.unblockUI();
                if (response) {
                    if (response.rc == -2) {
                        // -- User must change the password at next logon.
                        $("#userid2").val($("#userid").val());
                        $("#subtitle").html(thinRDPconsts.passwordExpire);
                        $("#login-page").hide();
                        $("#changePassword-page").show();
                        //alertPopup(thinRDPconsts.passwordExpire, thinRDPconsts.productDescription, undefined, undefined, undefined, function () {
                        //    setTimeout(function () {
                        //        unblockPopup();
                        //        $("#oldPassword").focus();
                        //    }, 200);
                        //});
                        return;
                    } else if (response.rc == -1) {
                        // -- Invalid user.
                        $("#message").html(thinRDPconsts.invalidUserPass);
                    } else if (response.msg) {
                        $("#message").html(response.msg);
                    } else {
                        $("#message").html( thinRDPconsts.invalidUserPass);
                    }
                }

            });
    }

    function doChangePassword() {
        var username = $("#userid2").val();
        var oldPassword = $("#oldPassword").val();
        var newPassword = $("#newPassword").val();
        var cnfPassword = $("#cnfPassword").val();

        var errorRequestPasswordAgain = function (msg) {
            $("#newPassword").val("");
            $("#cnfPassword").val("");
            alertPopup(msg, thinRDPconsts.productDescription, undefined, undefined, undefined, function () {
                setTimeout(function () {
                    unblockPopup();
                    $("#newPassword").focus();
                }, 200);
            });
        };
        if ((newPassword == cnfPassword)) {
            $.blockUI({
                message: "<div class='popup' id='updatingPassword'><h2>" + thinRDPconsts.pleaseWait + "</h2><img id='ecWaiting' src='images/core/loadajax.gif' /></div>",
                css: {
                    'padding': 0, 'margin': 0,
                    'width': '100%', 'height': '100%',
                    'top': '0px', 'left': '0px',
                    'textAlign': 'center',
                    'backgroundColor': 'transparent',
                    'border': 'none',
                    'cursor': 'auto'
                }, centerX: true, centerY: true
            });
            mythinrdp.changePassword(username, oldPassword, newPassword, function (obj) {
                $.unblockUI();
                if ((obj != undefined)) {
                    if (obj.rc == 0) {
                        $.unblockUI();
                        alertPopup(thinRDPconsts.passswordUpdated, thinRDPconsts.productDescription, undefined, undefined, undefined, function () {
                            setTimeout(function () {
                                unblockPopup();
                                $("#password").focus();
                            }, 200);
                        });
                        $("#changePassword-page").hide();
                        $("#subtitle").html(thinRDPconsts.enterCredentials);
                        $("#login-page").show();
                        $("#userid").val(username);
                    } else {
                        var msg = thinRDPconsts.changePasswordError;
                        if ((obj.msg != undefined) && (obj.msg != '')) {
                            msg = obj.msg;
                        }
                        errorRequestPasswordAgain(msg);
                    }
                } else {
                    var msg = thinRDPconsts.changePasswordError;
                    errorRequestPasswordAgain(msg);
                }
            }, function (error) {
                $.unblockUI();
                if (error != undefined) {
                    var msg = thinRDPconsts.changePasswordError;
                    if ((error.msg != undefined) && (error.msg != '')) {
                        msg = error.msg;
                    }
                    errorRequestPasswordAgain(msg);
                } else {
                    errorRequestPasswordAgain(thinRDPconsts.changePasswordError);
                }
            });
        } else {
            errorRequestPasswordAgain(thinRDPconsts.invalidPasswordMatch);
        }
    };

    // -- Release previous blockUI
    $.blockUI.defaults.fadeOut = 0;

    // -- If we can use the new method, we use it
    mythinrdp.getLoginMethods(function (methods, currentMethod) {

        function oauthLogin(url) {
            var expDate = new Date(); expDate.setSeconds(expDate.getSeconds() + 5);
            document.cookie = helper.string.format(cookiePrefix + "_AUTHENTICATING={0}; expires={1}; path={2}", url, expDate.toUTCString(), '/');
            location.href = url;
        }

        $("#secMethod").html("");
        $("#secMethodRow").hide();
        var throwsError = helper.utils.getCookie(cookiePrefix + "_AUTHENTICATING") != null;
        if ((typeof methods != "undefined") && (methods.length != 0)) {
            if (methods.length == 1 && methods[0].url && !throwsError) {
                oauthLogin(methods[0].url);
                return;
            }
            $(".header, .footer").show();
            var intCount = 0; // internal methods
            var extCount = 0; // external methods
            for (var m = 0; m < methods.length; m++) {
                var method = methods[m];
                if (!method.url) {
                    $("#secMethod").append(new Option("Use " + method.name, method.name, false, (methods.length == 1) ? true : method.name == currentMethod));
                    intCount++;
                } else {
                    $("#externals").append('<div class="row">' +
                            '<a class="button loginbtn lnkButton" id="' + method.id.toLowerCase() + '" href="' + method.url + '">' +
                                '<div class="imgbtn">&nbsp;</div>' +
                                '<div class="labelbtn">Login with ' + method.name + '</div>' +
                            '</a>' +
                        '</div>');
                    extCount++;
                }
            }
            if (intCount > 0) {
                if (intCount != 1) {
                    $("#secMethodRow").show();
                }
                $("#login-page").show();
            }
            if (extCount != 0) {
                $("#login-security").show();
            }
            if (intCount != 0 && extCount != 0) {
                $(".separator").show();
                $("h2#subtitle").html(thinRDPconsts.signInOrSelect);
            } else if (intCount != 0) {
                $("h2#subtitle").addClass("centered").html(thinRDPconsts.enterCredentials);
            } else {
                $("h2#subtitle").addClass("centered").html(thinRDPconsts.signInOption);
            }
        }
        if (throwsError) {
            $("#message").html(consts.unknownUser);
        }
    });

    if (window.location.search == "?signin") {
		$("#btnCancel").click(function() {
			window.location.href = window.location.href;
		});
        $("#authPane").show();
    }

    // -- Add 'signin' entry to the browser history.
    // -- BUG4723 - Reverse proxy
    window.history.pushState('signin', 'Sign in', helper.url.parseUrl(window.location.href).toUrl('-a -r'));
    // -- BUG4723 - Reverse proxy

    // -- Add event to loginOk Element
    $("#loginOk").click(doLogin);

    $("#changePasswordOk").click(doChangePassword);

    // -- Set the ENTER key as default login.
    $(document.body).keydown(function (event) {
        if (event.keyCode == "13") {
            doLogin();
        }
    });

    // -- After html is ready, set focus to userId element.
    window.setTimeout(function () { $("#userid").focus(); }, 100);
});