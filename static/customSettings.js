/* Thinfinity(c) Remote Desktop Server v4.0.2.0 */
var customSettings = {
    /*
    "createToolbar": true,                             // Create ThinRDP toolbar
    "toolbarVisible": false,                           // ThinRDP toolbar starts expanded (visible)
    "disableDisconnect": false,                        // Enables/disables the "disconnect" toolbar button
    "checkBeforeWindowClose": true,                    // when false, skips the user confirmation popup of the onBeforeUnload event
    "noSsnDialog": false,                              // avoids the share session popup dialog
    "noShowPopupsOnClose": false,                      // when true, skips the session closed message popup
    "mobile":{
        "extendedKeyboard": {
            "enabled": true,
            "keys": {
                "F1": false, "F2": true, "F3": true, "F4": true, "F5": true, "F6": true, "ESC": true, "Print": true, "Srclck": true, "Pause": true, "NumL": true,
                "F7": true, "F8": true, "F9": true, "F10": true, "F11": true, "F2": true, "PgUp": true, "PgDn": true, "Up": true, "Return": true,
                "WinKey": true, "CtrlAltDel": true, "Computer": true, "Tab": true, "Left": true, "Bottom": true, "Right": true,
                "Ctrl": true, "Alt": true, "Shift": true, "Del": true, "BackSpace": true, "Home": true, "End": true, "Next": true
            }
        },
        "shortcuts": {
            "CtrlAltDel": true
        }
    }
     */
};
