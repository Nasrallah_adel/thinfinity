﻿// =============================================================================================================================
// Copyright 2016, CybeleSoft
// CYBELESOFT
// 20170712.1
// info@cybelesoft.com
// =============================================================================================================================
// -- Required:
// --   helper.js
// *****************************************************************************************************************************
// FONT PLUGIN CLASS ***********************************************************************************************************
// *****************************************************************************************************************************
(function () {
    function Font() {
        // -- PRIVATE VARIABLES ************************************************************************************************
        var _ref = this;
        var _anchor = null;
        var _font = {
            'separation': 1,
            'default': {
                'size': 12,
                'style': 'normal',
                'weight': 'normal',
                'family': 'Arial'
            }
        };
        // -- END PRIVATE VARIABLES ********************************************************************************************
        // -- PRIVATE METHODS **************************************************************************************************
        function initialize() {
            try {
                //Attach anchor to document to calculate each size;
                if (window.document && window.document.body) {
                    _anchor = document.createElement('a');
                    _anchor.style.position = 'absolute';
                    _anchor.style.top = '-5000px';
                    _anchor.style.left = '-5000px';
                    window.document.body.appendChild(_anchor);
                } else {
                    setTimeout(initialize, 200);
                }
            } catch (error) { return error; }
        };
        function getSize(text, font) {
            try {
                var width = 0;
                var height = 0;
                font = validateFont(font);
                var fs = font.size;
                for (var index = 0; index < text.toString().length; index++) {
                    var letter = text.toString().substring(index + 1, index);
                    var size = calcSize(letter, font);
                    width += size.width + _font.separation;
                    height = (size.height > height) ? size.height : height;
                }
                return { 'width': width, 'height': height };
            } catch (error) { return error; }
        };
        function getWidth(text, font) {
            try {
                var width = 0;
                font = validateFont(font);
                var fs = font.size;
                for (var index = 0; index < text.toString().length; index++) {
                    var letter = text.toString().substring(index + 1, index);
                    var w = calcSize(letter, font).width;
                    width += w + _font.separation;
                }
                return width;
            } catch (error) { return error; }
        };
        function getHeight(text, font) {
            try {
                var height = 0;
                font = validateFont(font);
                var fs = font.size;
                for (var index = 0; index < text.toString().length; index++) {
                    var letter = text.toString().substring(index + 1, index);
                    var h = calcSize(letter, font).height;
                    height = (h > height) ? h : height;
                }
                return height;
            } catch (error) { return error; }
        };
        function getPhraseSize(text, font) {
            try {
                var width = 0;
                var height = 0;
                font = validateFont(font);
                var fs = font.size;
                var size = { 'width': 0, 'height': 0 };
                if (typeof (text) == 'string') {
                    //Get all '<br/>' and '\n\r'
                    var pattern = /<br>|<br\/>|\\n\\r|\\\\n\\\\r|\\n|\\\\n/g;
                    var m = text.match(pattern);
                    var newText = text;
                    if (m != null) {
                        var s = text.split(pattern);
                        var w = 0;
                        var h = 0;
                        for (var index in s) {
                            var temp = calcSize(s[index], font);
                            if (temp.width > w) { w = temp.width; }
                            if (temp.height > h) { h = temp.height; }
                        }
                        size.width = w;
                        size.height = h * s.length;
                    } else { size = calcSize(newText, font); }
                }
                return size;
            } catch (error) { return error; }
        };
        function getValue(value) {
            try {
                if (helper.object.isNumber(value)) { return value; }
                else if (typeof (value == 'string')) {
                    var re = new RegExp('[0-9]*');
                    var regExpExec = re.exec(value);
                    if ((regExpExec != null) && (regExpExec[0] != undefined) && (regExpExec[0] != '') && (regExpExec[0] != null)) { return helper.object.convertTo(regExpExec[0], 'int'); }
                }
            } catch (error) { return _font.default.size; }
        }
        function validateFont(font) {
            try {
                if ((font == null) || (font == undefined)) { font = {}; }
                else { font = helper.object.clone(font); }
                var dSize = _font.default.size;
                var dStyle = _font.default.style;
                var dFontName = _font.default.family;
                var dWeight = _font.default.weight;
                for (var prop in font) {
                    switch (prop.toUpperCase()) {
                        case 'SIZE':
                        case 'FONTSIZE':
                            font.size = font[prop];
                            if (helper.object.isNumber(font.size) == true) { font.size += 'px'; }
                            break;
                        case 'NAME':
                        case 'FONTNAME':
                            font.name = font[prop];
                            break;
                        case 'WEIGHT':
                        case 'FONTWEIGHT':
                            font.weight = font[prop];
                            break;
                        case 'STYLE':
                        case 'FONTSTYLE':
                            font.style = font[prop];
                            break;
                    }
                }
                if (font.name == undefined) { font.name = dFontName; }
                else { font.name = font.name.toLowerCase(); }
                if (font.style == undefined) { font.style = dStyle; }
                else { font.style = font.style.toLowerCase(); }
                if (font.weight == undefined) { font.weight = dWeight; }
                else if (helper.object.isNumber(font.weight) == false) {
                    font.weight = font.weight.toLowerCase();
                }
                if (font.size == undefined) { font.size = dSize; }
                else { font.size = getValue(font.size); }
                if (font.size % 2 != 0) { font.size++; }
            } catch (error) { return error; }
            return font;
        };
        function calcSize(text, font) {
            try {
                if (_anchor == null) { initialize(); }
                _anchor.style.fontFamily = font.name;
                _anchor.style.fontSize = font.size + 'px';
                _anchor.style.fontStyle = font.style;
                if (_anchor.style.fontWeight) {
                    _anchor.style.fontWeight = font.weight;
                }
                _anchor.innerHTML = text;
                var size = helper.size.get(_anchor);
                return size;
            } catch (error) { return error; }
        };
        // -- END PRIVATE METHODS **********************************************************************************************
        initialize();
        // -- PUBLIC METHODS ***************************************************************************************************
        Object.defineProperty(this, 'get', { 'enumerable': true, 'configurable': false, 'writeable': false, 'value': getSize });
        Object.defineProperty(this, 'getWidth', { 'enumerable': true, 'configurable': false, 'writeable': false, 'value': getWidth });
        Object.defineProperty(this, 'getHeight', { 'enumerable': true, 'configurable': false, 'writeable': false, 'value': getHeight });
        Object.defineProperty(this, 'getPhraseSize', { 'enumerable': true, 'configurable': false, 'writeable': false, 'value': getPhraseSize });
        helper.console.log(helper.string.format('Initializing {0} class - {1}', 'Font', new Date().toString()), arguments);
        // -- END PUBLIC METHODS ***********************************************************************************************
        return this;
    };
    if (typeof window.helperPlugins == 'undefined') { helperPlugins = {}; }
    // -- Can force to create a new instance each time.
    // helperPlugin.Font = function(args){return new Font(); }
    helperPlugins.Font = Font;
    if (typeof window.helper != 'undefined') {
        helper.registerPlugin('font', Font);
    }
})();
// *****************************************************************************************************************************
// END FONT PLUGIN CLASS *******************************************************************************************************
// *****************************************************************************************************************************